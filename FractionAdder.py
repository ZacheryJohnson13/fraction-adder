import os, math

validEntries = ['1','2','3','4','5','6','7','8','9','/']
inputs = []
fNum = []
fDen = []


def main():
	os.system('clear')
	print("Enter fractions. Hit enter to submit, and enter on a blank line to add. \n")
	GetUserInput()

def GetUserInput():
	userInput = input(">")

	#Fractionify whole numbers
	if '/' not in userInput:
		userInput += "/1"

	#If we are ready to parse
	if userInput == "/1":
		ParseFractions()
	else:
		slashCount = 0
		stringLength = len(userInput)
		for i in range(0, stringLength):
			if(userInput[i] is '/'):
				slashCount += 1
		if slashCount > 1:
			ThrowError("Invalid fraction syntax.")
		else:
			inputs.append(userInput)
			GetUserInput()


def ParseFractions():
	print("Parsing fractions")
	for frac in range(0, len(inputs)):
		fractionSplit = inputs[frac].split('/')
		fNum.append(fractionSplit[0])
		fDen.append(fractionSplit[1])

	AddFractions()

def AddFractions():
	#Find common denominator
	
	uniqueDen = []
	finalDen = 0

	finalDen = FindCommonDenominator(fDen)

	AssembleAnswer(finalDen)

def FindCommonDenominator(numbers):
	totalFactors = []

	#Find prime factorization for each individual number/denominator
	for i in range(0, len(numbers)):
		n = int(numbers[i])
		numberFactors = []

		while (n % 2 == 0):
			numberFactors.append(2)
			n = n/2
		for x in range(3, math.ceil(math.sqrt(n))):
			while (n % x == 0):
				numberFactors.append(x)
				n = n/x
		if n > 2:
			numberFactors.append(int(n))

		factorString = ""
		for num in range(0, len(numberFactors)):
			factorString += (str(numberFactors[num]) + " ")

		totalFactors.append(factorString)

	#Find the least common denominator using the factorization acquired above
	highestPowerFactors = {}
	tempDictionary = {}

	#Total number of factor strings to parse
	for i in range(0, len(totalFactors)):
		string = totalFactors[i]
		tempDictionary.clear()

		#Length of first factor string
		for x in range(0, len(string)):
			if str(string[x]) != " ":
				if str(string[x]) in tempDictionary:
					tempDictionary[str(string[x])] += 1
				else:
					tempDictionary[str(string[x])] = 1

		for z in range(0, len(tempDictionary)):
			factorKey = list(tempDictionary.keys())[z]

			if factorKey in highestPowerFactors:
				if tempDictionary[factorKey] > highestPowerFactors[factorKey]:
					highestPowerFactors[factorKey] = tempDictionary[factorKey]
			else:
				highestPowerFactors[factorKey] = tempDictionary[factorKey]

	commonFactor = 0
	for i in range(0, len(highestPowerFactors)):
		print("Key: " + str(list(highestPowerFactors.keys())[i]) + " Instances: " + str(list(highestPowerFactors.values())[i]))
		if commonFactor == 0:
			commonFactor = int(list(highestPowerFactors.keys())[i]) * int(list(highestPowerFactors.values())[i])
		else:
			commonFactor *= int(list(highestPowerFactors.keys())[i]) * int(list(highestPowerFactors.values())[i])

	print("Common Denominator of " + str(commonFactor))
	return commonFactor

def AssembleAnswer(denominator):
	finalNumerators = []
	for i in range(0, len(inputs)):
		multiplier = int(int(denominator) / int(fDen[i]))
		finalNumerators.append(int(fNum[i]) * multiplier)

	finalNum = 0
	for i in range(0, len(finalNumerators)):
		finalNum += finalNumerators[i]

	print("Answer is " + str(finalNum) + "/" + str(denominator)) 

def ThrowError(errorMessage):
	os.system('clear')
	print(errorMessage)


main()


